# EcoSystems Game Server

The official server for the EcoSystems game. This application includes 2 Core Libraries, System and Network. It makes use of the System CL much in the same way that the Client Does. The server has a GameLoop and Scene system. However, it does not contain any sort of Graphics Rendering System.

Clone With Submodules:
```
git clone --recurse-submodules https://gitlab.com/ecosystems-game/applications/ecosystems-server.git
```

## Requirements & Dependencies

```
EcoSystems-Server:
    - CMake Version >= 3.16
    - EcoSystems-Network: (Submodule)
        - CMake Version >= 3.16
        - Boost Version >= 1.71.0
            (Asio, System, FileSystem)
    - EcoSystems-System: (Submodule)
        - GLM >= 0.9.9.8 (Included in ThirdParty)
```