#ifndef ECOSYSTEMS_INCOMINGCHATEVENT_HPP
#define ECOSYSTEMS_INCOMINGCHATEVENT_HPP

#include <boost/asio.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <string>
#include "network/event_system/NetworkEvent.hpp"
#include "network/Packet.hpp"
#include "network/Session.hpp"
#include <chrono>
#include <thread>

namespace ecosystems::server {
    class IncomingChatEvent : public network::events::NetworkEvent {
    public:
        IncomingChatEvent() : network::events::NetworkEvent() {}
        void Call(network::Packet& p_packet, std::shared_ptr<network::Session>& p_session, std::shared_ptr<network::events::NetworkObserver> p_observer) override {
            std::cout << "============-- Network Test --============" << std::endl;
            std::cout << "============ Incoming Message ============" << std::endl;

            switch (p_packet.get_send_method()) {
                case network::UDP:
                    std::cout << "========= Testing Protocol [UDP] =========" << std::endl;
                    break;
                case network::TCP:
                    std::cout << "========= Testing Protocol [TCP] =========" << std::endl;
                    break;
                case network::LOCAL:
                    std::cout << "========= Testing Protocol [LOCAL] =========" << std::endl;
                    break;
                default:
                    std::cerr << "It literally is not possible to get here. Received an Unknown Protocol somehow?" << std::endl;
                    break;
            }

            std::cout << "If you made it this far, you know your server is receiving messages on the protocol above." << std::endl;
            std::cout << std::endl << std::endl; // Just some spaces to be fancy

            std::cout << "[Session Connection Test]" << std::endl;
            std::cout << "    >> Next we'll test Session Binding functionality between UDP and TCP." << std::endl;
            std::cout << "    >> By this point, the network should have already established a TCP Connection, and linked it to a UDP Endpoint" << std::endl;
            std::cout << "    >> When the client connections, a session is created, the id is sent to the client, the client stores the id." << std::endl;
            std::cout << "    >> Afterwards, a Reliable UDP Packet is sent from the client to the server containing the SessionId of the client." << std::endl;
            std::cout << "    >> The server then stores uses the SessionId to find the established session, and binds UDP endpoint the packet was received from to the session." << std::endl;
            std::cout << "    >> If the UDP and TCP Endpoints and Session UUID are all printed, everything is hooked up." << std::endl;
            std::cout << "    >> Both UDP and TCP Endpoints should display regardless of the protocol this packet was received from." << std::endl;
            std::cout << std::endl;

            boost::uuids::uuid SESSION_UNASSIGNED_UUID = boost::uuids::string_generator()("00000000-0000-0000-0000-000000000000");
            if(p_session->get_session_uuid() != SESSION_UNASSIGNED_UUID && p_session->is_tcp_connected() && p_session->is_udp_connected()) {
                std::cout << "It worked, Here are your results... :)" << std::endl << std::endl;
                std::string udp_endpoint = p_session->get_udp_endpoint().address().to_string() + ":" + std::to_string(p_session->get_udp_endpoint().port());
                std::string tcp_endpoint = p_session->get_tcp_socket().remote_endpoint().address().to_string() + ":" + std::to_string(p_session->get_tcp_socket().remote_endpoint().port());

                std::cout << "    UDP Endpoint: " << udp_endpoint << std::endl;
                std::cout << "    Session ID: " << p_session->get_session_uuid() << std::endl;
                std::cout << "    TCP Endpoint" << tcp_endpoint << std::endl;
            } else {
                std::cerr << "Well that's embarrassing, the Session isn't fully connected :c" << std::endl;
                std::cerr << "    session/is_tcp_connected: " << p_session->is_tcp_connected() << std::endl;
                std::cerr << "    session/is_udp_connected: " << p_session->is_udp_connected() << std::endl;
                std::cerr << "    session/get_session_uuid: " << p_session->get_session_uuid() << std::endl;
            }
            std::cout << std::endl << std::endl;

            // Validates that strings work
            auto message = p_packet.ReadNextArgument<std::string>();
            auto ipsum = p_packet.ReadNextArgument<std::string>();
            std::cout << "Message: " << message << std::endl;
            std::cout << "We also got the ipsum :3" << std::endl;
            std::cout << ipsum << std::endl;
            std::string validation_message("Hello Network!");

            if(message != validation_message) {
                std::cerr << "It appears a message came in corrupt... Make sure you're sending the first parameter as an std::string(\"Hello Network!\")" << std::endl;
                return; // Let's stop the test here.
            }

            std::cout << std::endl;
            std::cout << "Now, let's just send a validation test message back to the client..." << std::endl;

            std::this_thread::sleep_for(std::chrono::microseconds(2500000));
            network::Packet test_packet(p_observer->get_hook_index("player-chat"));
            test_packet.AddArgument("Hello Network!");
            test_packet.AddArgument("LOREM IPSUM!");
            test_packet.AddArgument<int32_t>(32);
            test_packet.AddArgument<float>(0.5f);

            switch (p_packet.get_send_method()) {
                case network::UDP:
                    p_session->UDP_Send(test_packet);
                    break;
                case network::TCP:
                    p_session->TCP_Send(test_packet);
                    break;
                case network::LOCAL:
                    std::cout << "========= Testing Protocol [LOCAL] =========" << std::endl;
                    break;
                default:
                    std::cerr << "It literally is not possible to get here. Received an Unknown Protocol somehow?" << std::endl;
                    break;
            }

//            std::cout << "Integer Arg3: " << x << std::endl;
//            std::cout << "Float Arg4: " << y << std::endl;
        }
    };
}

#endif //ECOSYSTEMS_INCOMINGCHATEVENT_HPP
